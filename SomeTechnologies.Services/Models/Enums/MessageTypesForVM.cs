﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomeTechnologies.Services.Models.Enums
{
    public enum MessageTypesForVM
    {
        userMessage,
        /// <summary>
        /// let This one incluses support and sevices  
        /// </summary>
        servicemessage,
    }
}

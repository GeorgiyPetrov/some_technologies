﻿using AutoMapper;
using DAL.Models;
using DAL.Models.Enums;
using SomeTechnologies.Services.Models.ChatModels;
using SomeTechnologies.Services.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomeTechnologies.Services.Mapper
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<Message, ChatMessageVM>()
                .ForMember(mess => mess.PostDate, vm => vm.MapFrom(e=>e.SendDate))
                .ReverseMap();

            // Map enum 2 values from source to 1
            CreateMap<MessageTypes, MessageTypesForVM>()
                .ConvertUsing((src, dist) => {
                    if (src == MessageTypes.ChatMessage) return MessageTypesForVM.userMessage;
                    else return MessageTypesForVM.servicemessage;
                });

        }
    }
}

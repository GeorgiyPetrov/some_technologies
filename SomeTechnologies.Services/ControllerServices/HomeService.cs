﻿using AutoMapper;
using DAL.Models;
using SomeTechnologies.Services.Models.ChatModels;
using System;
using System.Threading.Tasks;

namespace SomeTechnologies.Services.ControllerServices
{
    public class HomeService : IHomeService
    {
        private readonly IMapper _mapper;

        public HomeService(
            IMapper mapper)
        {
            _mapper = mapper;
        }

        /// <inheritdoc />
        public async Task<ChatMessageVM> GetChatMessageVM()
        {
            var dalModel = new Message()
            {
                Body = "Some text",
                ChatId = 1,
                MessageId = 1,
                SendDate = DateTime.Now,
                Type = DAL.Models.Enums.MessageTypes.ChatMessage
            };

            var vm = _mapper.Map<ChatMessageVM>(dalModel);
            dalModel.Type = DAL.Models.Enums.MessageTypes.SupportMessage;
            var xm = _mapper.Map<ChatMessageVM>(dalModel);

            return vm;
        }
    }
}

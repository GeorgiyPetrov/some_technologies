﻿using SomeTechnologies.Services.Models.ChatModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomeTechnologies.Services.ControllerServices
{
    public interface IHomeService
    {
        /// <summary>
        /// Now returns mapped model for example
        /// </summary>
        /// <returns></returns>
        Task<ChatMessageVM> GetChatMessageVM();
    }
}

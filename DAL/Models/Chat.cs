﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    /// <summary>
    /// Chat between some users
    /// </summary>
    public class Chat
    {

        public int ChatId { get; set; }

        /// <summary>
        /// Date of cration of this chat
        /// </summary>
        public DateTime CreationDate { get; set; }


        public List<Message> Messages { get; set; }
    }
}

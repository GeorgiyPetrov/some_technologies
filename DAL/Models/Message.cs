﻿using DAL.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    /// <summary>
    /// Message DB
    /// </summary>
    public class Message
    {
        public int MessageId { get; set; }

        public int ChatId { get; set; }

        /// <summary>
        /// Chat wich contains this message
        /// </summary>
        public Chat Chat { get; set; }

        /// <summary>
        /// Text of this message
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Sender ID
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Date time when this message was send
        /// </summary>
        public DateTime SendDate { get; set; }

        /// <summary>
        /// When it was read
        /// </summary>
        public DateTime? ReadDate { get; set; }

        /// <summary>
        /// Category of message
        /// </summary>
        public MessageTypes Type { get; set; }
    }
}

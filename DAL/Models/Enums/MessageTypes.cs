﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models.Enums
{
    /// <summary>
    /// Types of messages
    /// </summary>
    public enum MessageTypes
    {
        /// <summary>
        /// Message in chat
        /// </summary>
        ChatMessage,

        /// <summary>
        /// Message in support chat
        /// </summary>
        SupportMessage,

        /// <summary>
        /// Message from system
        /// </summary>
        SystemMessage
    }
}

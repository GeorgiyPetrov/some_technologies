﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using SomeTechnologies.Services.Models.ChatModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomeTechnologies.Controllers
{
    public class DataController : ControllerBase
    {
        private readonly IHostEnvironment _hostEnvironment;


        public DataController(IHostEnvironment hostEnvironment)
        {
            _hostEnvironment = hostEnvironment;
        }

        [HttpPost]
        public async Task<IActionResult> GetJson(string stringValue, IFormFile currentFile) {

            var path = Path.Combine(_hostEnvironment.ContentRootPath, "UploadedFiles");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            path = Path.Combine(path, currentFile.FileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await currentFile.CopyToAsync(stream);
            }

            var model = new ChatMessageVM()
            {
                Body = "stringValue seccessfuly saved",
                PostDate = DateTime.Now,
                Type = Services.Models.Enums.MessageTypesForVM.userMessage,
                UserId = "213"
            };
            return Ok(JsonConvert.SerializeObject(model));
        }
    }
}
